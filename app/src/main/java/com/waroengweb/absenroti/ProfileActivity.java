package com.waroengweb.absenroti;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.tapadoo.alerter.Alerter;
import com.waroengweb.absenroti.db.AppDatabase;
import com.waroengweb.absenroti.model.Users;
import com.waroengweb.absenroti.utils.AppHelper;
import com.waroengweb.absenroti.utils.Config;
import com.waroengweb.absenroti.utils.HttpRequest;
import com.waroengweb.absenroti.utils.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import androidx.room.Room;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends AppCompatActivity {

    Calendar myCalendar;
    String jenisKelamin;
    AppDatabase db;
    String photo;

    @BindView(R.id.profile_image_2)
    ImageView profileImage;
    @BindView(R.id.txtNama)
    TextView namaLengkap;
    @BindView(R.id.edt_Tanggal_Lahir)
    EditText tanggalLahir;
    @BindView(R.id.edt_Phone)
    EditText phone;
    @BindView(R.id.edt_Alamat)
    EditText alamat;
    @BindView(R.id.edt_Email)
    EditText email;
    @BindView(R.id.radio_jk)
    RadioGroup radioJk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().setTitle("Update Profile");

        ButterKnife.bind(this);

        myCalendar = Calendar.getInstance();

        tanggalLahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDateDialog();
            }
        });

        db = Room.databaseBuilder(this,
                AppDatabase.class, "MyDB").allowMainThreadQueries().fallbackToDestructiveMigration().build();



        radioJk.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                View radioButton = radioJk.findViewById(checkedId);
                int index = radioJk.indexOfChild(radioButton);
                switch (index) {
                    case 0:
                        jenisKelamin = "P";
                        break;
                    case 1:
                        jenisKelamin = "L";
                        break;

                }
            }
        });
        getProfile();
    }

    void getProfile() {

            Users user = db.UserDao().getUser(Session.getnik(this));
            Glide.with(ProfileActivity.this).load(user.getPhoto()).circleCrop().into(profileImage);
            namaLengkap.setText(user.getNama());
            email.setText(user.getEmail());
            phone.setText(user.getNo_telp());
            alamat.setText(user.getAlamat());
            tanggalLahir.setText(user.getTanggalLahir());
            if (user.getJenis_kelamin() != null) {
                if (user.getJenis_kelamin().equals("P")) {
                    ((RadioButton) radioJk.getChildAt(0)).setChecked(true);
                } else {
                    ((RadioButton) radioJk.getChildAt(1)).setChecked(true);
                }
            }
            photo = user.getPhoto();

    }


    void openDateDialog() {
        new DatePickerDialog(ProfileActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String formatTanggal = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(formatTanggal);
                tanggalLahir.setText(sdf.format(myCalendar.getTime()));

            }
        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnClick(R.id.btn_profile_submit)
    void updateProfile() {

        Users user = new Users();
        user.setNik(Session.getnik(this));
        user.setEmail(email.getText().toString());
        user.setAlamat(alamat.getText().toString());
        user.setNo_telp(phone.getText().toString());
        user.setTanggalLahir(tanggalLahir.getText().toString());
        user.setJenis_kelamin(jenisKelamin);
        user.setPhoto(photo);
        user.setNama(Session.getFullname(this));

        db.UserDao().insertUser(user);
        AppHelper.toast(this,"berhasil Update Profile","success");
    }


}