package com.waroengweb.absenroti.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.waroengweb.absenroti.db.dao.AbsenDao;
import com.waroengweb.absenroti.db.dao.UserDao;
import com.waroengweb.absenroti.model.Absen;
import com.waroengweb.absenroti.model.Users;

@Database(entities = {Absen.class, Users.class},version = 2,exportSchema = false)
@TypeConverters({DateTypeConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract AbsenDao AbsenDao();
    public abstract UserDao UserDao();
}
