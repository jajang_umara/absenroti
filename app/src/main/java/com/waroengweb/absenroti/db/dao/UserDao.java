package com.waroengweb.absenroti.db.dao;

import com.waroengweb.absenroti.model.Users;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface UserDao {
    @Query("SELECT * FROM users WHERE nik=:nik")
    Users getUser(String nik);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertUser(Users user);

    @Update
    void updateUser(Users user);

    @Query("UPDATE users SET photo=:photo WHERE nik=:nik")
    void updatePhoto(String photo,String nik);
}
