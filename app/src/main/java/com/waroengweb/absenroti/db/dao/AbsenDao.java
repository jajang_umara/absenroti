package com.waroengweb.absenroti.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.waroengweb.absenroti.model.Absen;

import java.util.List;

@Dao
public interface AbsenDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertAbsen(Absen absen);

    @Query("SELECT * FROM absen WHERE tanggal=:tanggal AND nik=:nik")
    List<Absen> getAbsenByDate(long tanggal,String nik);

    @Query("SELECT * FROM absen WHERE tanggal BETWEEN :tanggal1 AND :tanggal2 AND nik=:nik AND status=0 ORDER BY tanggal asc")
    Absen getAbsenMasuk(long tanggal1,long tanggal2,String nik);

    @Query("SELECT * FROM absen WHERE tanggal BETWEEN :tanggal1 AND :tanggal2 AND nik=:nik AND status=1 ORDER BY tanggal desc")
    Absen getAbsenPulang(long tanggal1,long tanggal2,String nik);
}
