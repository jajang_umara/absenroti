package com.waroengweb.absenroti;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tapadoo.alerter.Alerter;
import com.waroengweb.absenroti.utils.AppController;
import com.waroengweb.absenroti.utils.Config;
import com.waroengweb.absenroti.utils.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static maes.tech.intentanim.CustomIntent.customType;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.password)
    EditText password;

    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (Session.getLoginStatus(this)) {
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
        }

        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
    }

    public void slideUp(View view) {
        if (username.getText().toString().equals("")) {
            Alerter.create(this)
                    .setTitle("ERROR")
                    .setText("Username atau Password masih Kosong")
                    .setBackgroundColorInt(Color.RED).show();
            return;
        }

        doLogin();

        //startActivity(new Intent(LoginActivity.this, MainActivity.class));
        //customType(LoginActivity.this,"bottom-to-up");
    }

    public void doLogin() {
        pd.show();
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, Config.LOGIN_URL,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        pd.hide();
                        Log.d("Response :", response);
                        JSONObject jsonObject;
                        try {
                            jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("error") == false) {
                                JSONObject user = jsonObject.getJSONObject("data");
                                JSONObject karyawan = user.getJSONObject("karyawan");
                                JSONObject jabatan = karyawan.getJSONObject("jabatan");
                                Session.setLoginStatus(LoginActivity.this, true);
                                Session.setToken(LoginActivity.this, user.getString("token"));
                                Session.setRole(LoginActivity.this, user.getString("role"));
                                Session.setJabatan(LoginActivity.this, jabatan.getString("nama_jabatan"));
                                Session.setUserId(LoginActivity.this, user.getInt("id"));
                                Session.setFullname(LoginActivity.this, user.getString("nama"));
                                Session.setNik(LoginActivity.this, user.getString("nik"));
                                Session.setLokasi(LoginActivity.this, user.getString("lokasi"));
                                Session.setLokasiId(LoginActivity.this,user.getInt("id_lokasi"));
                                Session.setStatusPenilaian(LoginActivity.this,jabatan.getInt("status_penilaian"));
                                Session.setLatitude(LoginActivity.this,user.getDouble("latitude"));
                                Session.setLongitude(LoginActivity.this,user.getDouble("longitude"));
                                Session.setRadius(LoginActivity.this,user.getInt("radius"));
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                customType(LoginActivity.this, "bottom-to-up");
                            } else {
                                Alerter.create(LoginActivity.this)
                                        .setTitle("ERROR")
                                        .setText(jsonObject.getString("message"))
                                        .setBackgroundColorInt(Color.RED).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pd.hide();
                // TODO: Handle error
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user", username.getText().toString());
                params.put("password", password.getText().toString());
                return params;
            }
        };


        AppController.getInstance().addToRequestQueue(jsonObjectRequest);


    }
}