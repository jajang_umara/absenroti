package com.waroengweb.absenroti.model;

public class Toko {
    private int id;
    private String namaToko;

    public Toko(int id,String namaToko){
        this.id = id;
        this.namaToko = namaToko;
    }

    public int getId() {
        return id;
    }

    public String getNamaToko() {
        return namaToko;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNamaToko(String namaToko) {
        this.namaToko = namaToko;
    }
}
