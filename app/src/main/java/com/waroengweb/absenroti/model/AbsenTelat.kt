package com.waroengweb.absenroti.model

class AbsenTelat(
        var id: Int,
        var tanggal: String,
        var lokasitoko: String,
        var nama: String,
        var jabatan: String,
        var totalTelat: Int,
        var keterangan: String

) {
    override fun toString(): String {
        return nama
    }
}