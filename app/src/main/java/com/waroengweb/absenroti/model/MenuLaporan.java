package com.waroengweb.absenroti.model;

public class MenuLaporan {
    private int id;
    private String nama;
    private int laporan;

    public MenuLaporan(int id,String nama,int laporan){
        this.id = id;
        this.nama = nama;
        this.laporan = laporan;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getLaporan() {
        return laporan;
    }

    public void setLaporan(int laporan) {
        this.laporan = laporan;
    }
}
