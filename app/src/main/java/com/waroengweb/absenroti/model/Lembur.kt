package com.waroengweb.absenroti.model

class Lembur (
    var id: Int,
    var tanggal: String,
    var lokasiToko: String,
    var nama: String,
    var jabatan: String,
    var totalLembur: Int,
    var statusAcc: Int
)