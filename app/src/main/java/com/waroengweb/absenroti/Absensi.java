package com.waroengweb.absenroti;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.room.Room;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.android.SphericalUtil;
import com.tapadoo.alerter.Alerter;
import com.waroengweb.absenroti.db.AppDatabase;
import com.waroengweb.absenroti.model.Absen;
import com.waroengweb.absenroti.utils.AppHelper;
import com.waroengweb.absenroti.utils.Config;
import com.waroengweb.absenroti.utils.HttpRequest;
import com.waroengweb.absenroti.utils.Session;
import com.waroengweb.absenroti.utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;

public class Absensi extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap googleMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private int PERMISSION_ID = 444;
    private Uri filePhoto;
    private String fileString,jamMasuk,jamKeluar;
    private Marker marker;
    private Double latitude,longitude;
    private AppDatabase db;
    private ProgressDialog pd;

    @BindView(R.id.preview) ImageView imagePhoto;
    @BindView(R.id.take_picture) Button takePicture;
    @BindView(R.id.txt_tanggal) TextView txtTanggal;
    @BindView(R.id.proses_masuk) Button btnMasuk;
    @BindView(R.id.proses_pulang) Button btnKeluar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absensi);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        ButterKnife.bind(this);
        new AppHelper(this).setDate(txtTanggal);
        db = Room.databaseBuilder(this,
                AppDatabase.class, "MyDB").allowMainThreadQueries().fallbackToDestructiveMigration().build();
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        jamMasuk = getIntent().getStringExtra("jam_masuk");
        jamKeluar = getIntent().getStringExtra("jam_keluar");
        setButton();


    }

    void setButton() {
        if (jamMasuk == null ){
            btnMasuk.setEnabled(false);
        } else {
            if (!jamMasuk.equals("0:00")){
                btnMasuk.setEnabled(false);
            }
        }

        if (jamKeluar == null ){
            btnKeluar.setEnabled(false);
        } else {
            if (!jamKeluar.equals("0:00")){
                btnKeluar.setEnabled(false);
            }
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation()
    {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                mFusedLocationClient
                        .getLastLocation()
                        .addOnCompleteListener(
                                new OnCompleteListener<Location>() {
                                    @Override
                                    public void onComplete(
                                            @NonNull Task<Location> task)
                                    {
                                        Location location = task.getResult();
                                        if (location == null) {
                                            requestNewLocationData();
                                        }
                                        else {
                                            latitude = location.getLatitude();
                                            longitude = location.getLongitude();
                                            if(marker == null){
                                                LatLng latLng = new LatLng(latitude, longitude);
                                                MarkerOptions markerOptions = new MarkerOptions().position(latLng).title("I am here!");
                                                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                                                marker = googleMap.addMarker(markerOptions);

                                                if (Session.getLatitude(Absensi.this) != 0){
                                                    CircleOptions circleOptions = new CircleOptions();
                                                    circleOptions.center(new LatLng(Session.getLatitude(Absensi.this),Session.getLongitude(Absensi.this)));
                                                    circleOptions.radius(Session.getRadius(Absensi.this));
                                                    circleOptions.fillColor(Color.GREEN);
                                                    circleOptions.strokeWidth(1);
                                                    googleMap.addCircle(circleOptions);
                                                }

                                            }
                                        }
                                    }
                                });
            } else {
                Toast.makeText(this, "Please turn on" + " your location...", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        }
        else {
            requestPermissions();
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData()
    {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private LocationCallback mLocationCallback  = new LocationCallback() {

        @Override
        public void onLocationResult(
                LocationResult locationResult)
        {
            Location mLastLocation = locationResult.getLastLocation();
            if (marker != null){
                latitude = mLastLocation.getLatitude();
                longitude = mLastLocation.getLongitude();
                marker.setPosition(new LatLng(latitude,longitude));
            }
        }
    };


    private boolean checkPermissions()
    {
        return ActivityCompat
                .checkSelfPermission(
                        this,
                        Manifest.permission
                                .ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED

                && ActivityCompat
                .checkSelfPermission(
                        this,
                        Manifest.permission
                                .ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;

        // If we want background location
        // on Android 10.0 and higher,
        // use:
        /* ActivityCompat
                .checkSelfPermission(
                    this,
                    Manifest.permission
                        .ACCESS_BACKGROUND_LOCATION)
            == PackageManager.PERMISSION_GRANTED
        */
    }

    // method to requestfor permissions
    private void requestPermissions()
    {
        ActivityCompat.requestPermissions(
                this,
                new String[] {
                        Manifest.permission
                                .ACCESS_COARSE_LOCATION,
                        Manifest.permission
                                .ACCESS_FINE_LOCATION },
                PERMISSION_ID);
    }

    // method to check
    // if location is enabled
    private boolean isLocationEnabled()
    {
        LocationManager
                locationManager
                = (LocationManager)getSystemService(
                Context.LOCATION_SERVICE);

        return locationManager
                .isProviderEnabled(
                        LocationManager.GPS_PROVIDER)
                || locationManager
                .isProviderEnabled(
                        LocationManager.NETWORK_PROVIDER);
    }

    // If everything is alright then
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0
                    && grantResults[0]
                    == PackageManager
                    .PERMISSION_GRANTED) {

                getLastLocation();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkPermissions()) {
            getLastLocation();
        }
    }

    @OnClick(R.id.take_picture)
    public void takePicture() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent i;
            i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File pictureFile = null;
            try {
                pictureFile = new AppHelper(this).getOutputMediaFile();
            } catch (IOException ex) {
                Toast.makeText(this,
                        "Photo file can't be created, please try again",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            filePhoto = Uri.fromFile(pictureFile);
            Uri photoUri = FileProvider.getUriForFile(this,
                    "com.waroengweb.absenroti.fileprovider",pictureFile
            );
            i.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

            startActivityForResult(i,200);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if(resultCode == RESULT_OK){
            File compressPhoto = compressImage(filePhoto);
            //int file_size = Integer.parseInt(String.valueOf(compressPhoto.length()/1024));
            //Log.d("photo",file_size+" kb");
            fileString = (compressImage(filePhoto)).toString();
            imagePhoto.setImageURI(Uri.fromFile(compressPhoto));
            imagePhoto.requestFocus();
            takePicture.setText("Ganti Photo");
            getLastLocation();
        }

    }

    public File compressImage(Uri fileData){

        File compressFile;
        try {
            compressFile = new Compressor(this).compressToFile(new File(fileData.getPath()));
            return compressFile;
        } catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public void prosesAbsen(View view) {

        if (latitude == null || longitude == null ){
            Alerter.create(this).setTitle("ERROR").setText("LOKASI GPS MASIH KOSONG").setBackgroundColorInt(Color.RED).show();
            return;
        }

        Double dist = 0.0;
        if(Session.getLatitude(this) != 0){
            dist =  SphericalUtil.computeDistanceBetween(new LatLng(latitude,longitude),new LatLng(Session.getLatitude(this),Session.getLongitude(this)));
        }

        if (dist > Session.getRadius(this)){
            AppHelper.toast(this,"Tidak bisa Absen karena Posisi Diluar Toko","error");
            return;
        }


        if (fileString == null){
            Alerter.create(this).setTitle("ERROR").setText("BELUM AMBIL PHOTO..").setBackgroundColorInt(Color.RED).show();
            return;
        }
        int status;
        if (view.getId() == R.id.proses_masuk){
            status = 0;
        } else {
            status = 1;
        }

        Absen absen = new Absen();
        absen.setNik(Session.getnik(this));
        absen.setLatitude(latitude);
        absen.setLongitude(longitude);
        absen.setStatus(status);
        absen.setPhoto(fileString);
        absen.setUploaded(0);

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date tanggalNew;
        String tanggal = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        try {
            tanggalNew = formatter.parse(tanggal);
            absen.setTanggal(tanggalNew);
        } catch (ParseException pe){
            pe.printStackTrace();
        }

        //db.AbsenDao().insertAbsen(absen);

        pd.show();

        Map<String,String> params = new HashMap<>();
        params.put("nik",absen.getNik());
        params.put("status",String.valueOf(absen.getStatus()));
        params.put("latitude",String.valueOf(absen.getLatitude()));
        params.put("longitude",String.valueOf(absen.getLongitude()));

        Map<String, VolleyMultipartRequest.DataPart> files = new HashMap<>();
        long imagename = System.currentTimeMillis();
        files.put("photo",new VolleyMultipartRequest.DataPart("PIC_1_"+imagename+".jpg",new AppHelper(this).convertImageToBytes(absen.getPhoto())));

        HttpRequest.multiPartRequest(this, Config.ABSEN_URL, params,files, new HttpRequest.ResponseSuccess() {
            @Override
            public void onSuccess(String message, JSONObject json) {
                pd.hide();
                //Alerter.create(Absensi.this).setTitle("INFO").setText(message).setBackgroundColorInt(Color.GREEN).show();
                //fileString = null;
                //takePicture.setText("Ambil Photo");
                //imagePhoto.setImageDrawable(getResources().getDrawable(R.drawable.avatar));
                finish();
            }
        }, new HttpRequest.ResponseError() {
            @Override
            public void onError(String message) {
                pd.hide();
                Alerter.create(Absensi.this).setTitle("ERROR").setText(message).setBackgroundColorInt(Color.RED).show();
            }
        });


    }



}