package com.waroengweb.absenroti.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.waroengweb.absenroti.R;
import com.waroengweb.absenroti.model.AbsenTelat;
import com.waroengweb.absenroti.model.Image;
import com.waroengweb.absenroti.model.Lembur;
import com.waroengweb.absenroti.utils.Session;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LemburAdapter extends RecyclerView.Adapter<LemburAdapter.ViewHolder> {
    private Context context;
    private List<Lembur> absenList;
    private OnClickButton onClickButton;

    public LemburAdapter(Context context, List<Lembur> absenList){
        this.context = context;
        this.absenList = absenList;
    }

    public void setOnClickButton(OnClickButton onClickButton) {
        this.onClickButton = onClickButton;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.absen_lembur_item,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Lembur absen = absenList.get(position);
        holder.nama.setText(absen.getNama()+"("+absen.getTanggal()+")");
        holder.lokasi.setText("Lokasi : "+absen.getLokasiToko());
        holder.jabatan.setText("Jabatan : "+absen.getJabatan());
        holder.totalLembur.setText("Total Lembur : "+absen.getTotalLembur()+ " Menit");
        holder.keterangan.setText("Status : "+(absen.getStatusAcc() == 1 ? "Sudah Acc" : "Belum Acc"));
        holder.buttonAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickButton.onClickButton(absen,position);
            }
        });
        if (absen.getStatusAcc() == 1){
            holder.buttonAcc.setVisibility(View.GONE);
        }

        if (Session.getStatusPenilaian(context) == 0){
            holder.buttonAcc.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return absenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtName) TextView nama;
        @BindView(R.id.txtLokasi) TextView lokasi;
        @BindView(R.id.txtJabatan) TextView jabatan;
        @BindView(R.id.txtTotalLembur) TextView totalLembur;
        @BindView(R.id.txtStatus) TextView keterangan;
        @BindView(R.id.btnAcc) Button buttonAcc;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public interface OnClickButton {
        void onClickButton(Lembur lembur, int position);
    }
}
