package com.waroengweb.absenroti.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.waroengweb.absenroti.R;
import com.waroengweb.absenroti.model.Image;

import java.io.File;
import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.viewHolder> {

		Context context;
		List<Image> imageList;
		
		public static final int PERMISSION_WRITE = 0;
		private OnDeleteImage onDeleteImage;

		public ImageAdapter(Context context, List<Image> arrayList) {
				this.context = context;
				this.imageList = arrayList;
		}
		
		public void setOnDeleteImage(OnDeleteImage onDeleteImage) {
			this.onDeleteImage = onDeleteImage;
		}

		@Override
		public ImageAdapter.viewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
				 View view = LayoutInflater.from(context).inflate(R.layout.image_list, viewGroup, false);
				 return new viewHolder(view);
		}

		@Override
		public void onBindViewHolder(ImageAdapter.viewHolder viewHolder, final int position) {
				Glide.with(context)
						.load(new File(imageList.get(position).getImage()))
						.centerCrop()
						.into(viewHolder.image);
				viewHolder.delete.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						onDeleteImage.onClickButton(imageList.get(position),position);
					}
				});
		}

		@Override
		public int getItemCount() {
				return imageList.size();
		}

		public class viewHolder extends RecyclerView.ViewHolder {
				ImageView image, delete;
				public viewHolder(View itemView) {
						 super(itemView);
						image = (ImageView) itemView.findViewById(R.id.image);
						delete = (ImageView) itemView.findViewById(R.id.delete);
				}
		}
		
		public interface OnDeleteImage {
        void onClickButton(Image image,int position);
    }
}
