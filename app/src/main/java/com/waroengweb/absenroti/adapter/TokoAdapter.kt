package com.waroengweb.absenroti.adapter

import android.view.LayoutInflater
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.waroengweb.absenroti.R
import com.waroengweb.absenroti.model.Toko

class TokoAdapter(private val tokoList:List<Toko>,private val listener: TokoAdapter.OnClickCard) : RecyclerView.Adapter<TokoAdapter.ViewHolder>() {

    public class ViewHolder(view:View) : RecyclerView.ViewHolder(view) {
        val namaToko: TextView
        val cardToko: CardView
        init {
            namaToko = view.findViewById(R.id.toko_name)
            cardToko = view.findViewById(R.id.card_toko)
        }
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.menu_toko,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val toko = tokoList.get(position)
        holder.namaToko.setText(toko.namaToko)
        holder.cardToko.setOnClickListener {
            listener.onItemClick(toko)
        }
    }

    override fun getItemCount() = tokoList.size

    interface OnClickCard {
       fun onItemClick(toko:Toko)
    }
}