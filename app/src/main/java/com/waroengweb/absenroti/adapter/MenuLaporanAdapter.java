package com.waroengweb.absenroti.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.waroengweb.absenroti.R;
import com.waroengweb.absenroti.model.Image;
import com.waroengweb.absenroti.model.MenuLaporan;

import java.io.File;
import java.util.List;

public class MenuLaporanAdapter extends RecyclerView.Adapter<MenuLaporanAdapter.viewHolder> {

    Context context;
    List<MenuLaporan> menuList;

    public static final int PERMISSION_WRITE = 0;
    private OnClickCard onClickCard;

    public MenuLaporanAdapter(Context context, List<MenuLaporan> arrayList) {
        this.context = context;
        this.menuList = arrayList;
    }

    public void setOnClickCard(OnClickCard onClickCard){ this.onClickCard = onClickCard;}

    @Override
    public MenuLaporanAdapter.viewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.menu_laporan, viewGroup, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(MenuLaporanAdapter.viewHolder viewHolder, final int position) {
        MenuLaporan menu = menuList.get(position);
        viewHolder.nama.setText(menu.getNama());
        viewHolder.cardMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickCard.onClick(menu);
            }
        });

        if (menu.getLaporan() == 0){
            viewHolder.checklist.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.checklist.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        CardView cardMenu;
        TextView nama;
        ImageView checklist;
        public viewHolder(View itemView) {
            super(itemView);
            cardMenu = (CardView) itemView.findViewById(R.id.card_menu);
            nama = (TextView)itemView.findViewById(R.id.menu_name);
            checklist = (ImageView)itemView.findViewById(R.id.checklist);
        }
    }

    public interface OnClickCard {
        void onClick(MenuLaporan menuLaporan);
    }
}
