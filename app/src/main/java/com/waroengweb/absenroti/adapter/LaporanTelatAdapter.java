package com.waroengweb.absenroti.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.waroengweb.absenroti.R;
import com.waroengweb.absenroti.model.Absen;
import com.waroengweb.absenroti.model.AbsenTelat;
import com.waroengweb.absenroti.model.LaporanTelat;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LaporanTelatAdapter extends RecyclerView.Adapter<LaporanTelatAdapter.ViewHolder> {
    private Context context;
    private List<LaporanTelat> absenList;

    public LaporanTelatAdapter(Context context, List<LaporanTelat> absenList){
        this.context = context;
        this.absenList = absenList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.absen_telat_item,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final LaporanTelat absen = absenList.get(position);
        holder.nama.setText(absen.getNama()+"("+absen.getTanggal()+")");
        holder.lokasi.setText("Lokasi : "+absen.getLokasitoko());
        holder.jabatan.setText("Jabatan : "+absen.getJabatan());
        holder.totalTelat.setText("Total Telat : "+absen.getTotalTelat()+ " Menit");
        holder.keterangan.setText("Keterangan : "+(absen.getKeterangan().equals("null") ? "" : absen.getKeterangan()));
    }

    @Override
    public int getItemCount() {
        return absenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtName) TextView nama;
        @BindView(R.id.txtLokasi) TextView lokasi;
        @BindView(R.id.txtJabatan) TextView jabatan;
        @BindView(R.id.txtTotalTelat) TextView totalTelat;
        @BindView(R.id.txtKeterangan) TextView keterangan;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
