package com.waroengweb.absenroti.adapter

import android.view.LayoutInflater
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.waroengweb.absenroti.R
import com.waroengweb.absenroti.model.MyObject
import com.waroengweb.absenroti.model.Toko

class JabatanAdapter(private val list:List<MyObject>) : RecyclerView.Adapter<JabatanAdapter.ViewHolder>() {
    private var listener: JabatanAdapter.OnClickCard? = null

    public class ViewHolder(view:View) : RecyclerView.ViewHolder(view) {
        val nama: TextView
        val card: CardView
        init {
            nama = view.findViewById(R.id.jabatan_name)
            card = view.findViewById(R.id.card_jabatan)
        }
    }

    fun setOnClickListener(listener: JabatanAdapter.OnClickCard){
        this.listener = listener
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.menu_jabatan,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val toko = list.get(position)
        holder.nama.setText(toko.nama)
        holder.card.setOnClickListener {
            listener?.onItemClick(toko)
        }
    }

    override fun getItemCount() = list.size

    interface OnClickCard {
       fun onItemClick(myObject:MyObject)
    }
}