package com.waroengweb.absenroti.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.waroengweb.absenroti.R;
import com.waroengweb.absenroti.model.Absen;

import java.util.List;

import butterknife.ButterKnife;

public class AbsenAdapter extends RecyclerView.Adapter<AbsenAdapter.ViewHolder> {
    private Context context;
    private List<Absen> absenList;

    public  AbsenAdapter(Context context,List<Absen> absenList){
        this.context = context;
        this.absenList = absenList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.absen_item_list,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Absen absen = absenList.get(position);
    }

    @Override
    public int getItemCount() {
        return absenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
