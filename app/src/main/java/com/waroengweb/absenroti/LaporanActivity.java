package com.waroengweb.absenroti;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.waroengweb.absenroti.adapter.MenuLaporanAdapter;
import com.waroengweb.absenroti.model.MenuLaporan;
import com.waroengweb.absenroti.model.SubPoint;
import com.waroengweb.absenroti.utils.AppHelper;
import com.waroengweb.absenroti.utils.Config;
import com.waroengweb.absenroti.utils.HttpRequest;
import com.waroengweb.absenroti.utils.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LaporanActivity extends AppCompatActivity implements MenuLaporanAdapter.OnClickCard {

    @BindView(R.id.recyclerviewLaporan)
    RecyclerView recyclerView;

    private List<MenuLaporan> menuList = new ArrayList<>();
    private MenuLaporanAdapter adapter;
    private int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan);
        ButterKnife.bind(this);
        adapter = new MenuLaporanAdapter(this,menuList);
        adapter.setOnClickCard(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        id = getIntent().getIntExtra("id",1);
        getMenu();
    }

    void getMenu() {
        Map<String,String> params = new HashMap<>();
        params.put("id",String.valueOf(id));
        params.put("nik", Session.getnik(this));
        String tanggal = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        params.put("tanggal",tanggal);

        HttpRequest.stringRequest(this, Config.GET_MENU_URL, params, new HttpRequest.ResponseSuccess() {
            @Override
            public void onSuccess(String message, JSONObject json) throws JSONException {
                JSONArray rows = json.getJSONArray("rows");

                if (rows.length() > 0){
                    menuList.clear();
                    for(int i=0;i<rows.length();i++){
                        JSONObject row = rows.getJSONObject(i);
                        menuList.add(new MenuLaporan(row.getInt("id"),row.getString("keterangan"),row.getInt("laporan")));
                        adapter.notifyDataSetChanged();
                    }

                }

            }
        }, new HttpRequest.ResponseError() {
            @Override
            public void onError(String message) {
                AppHelper.toast(LaporanActivity.this,message,"error");
            }
        });
    }

    @Override
    public void onClick(MenuLaporan menuLaporan){
        Intent intent = new Intent(this,Dokumentasi.class);
        intent.putExtra("id_penilaian",menuLaporan.getId());
        intent.putExtra("jenis",menuLaporan.getNama());
        startActivity(intent);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        getMenu();
    }
}
