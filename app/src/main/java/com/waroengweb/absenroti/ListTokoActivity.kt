package com.waroengweb.absenroti

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.waroengweb.absenroti.adapter.TokoAdapter
import com.waroengweb.absenroti.model.Toko
import com.waroengweb.absenroti.utils.AppHelper
import com.waroengweb.absenroti.utils.Config
import com.waroengweb.absenroti.utils.HttpRequest
import com.waroengweb.absenroti.utils.Session
import kotlinx.android.synthetic.main.activity_list_toko.*
import org.json.JSONArray
import org.json.JSONObject

class ListTokoActivity : AppCompatActivity(),TokoAdapter.OnClickCard {
    val tokoList = ArrayList<Toko>()
    private lateinit var tokoAdapter:TokoAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_toko)
        supportActionBar?.setTitle("LIST LOKASI")
        tokoAdapter = TokoAdapter(tokoList,this)
        recyclerviewToko.apply {
            layoutManager = GridLayoutManager(this@ListTokoActivity,2)
            adapter = tokoAdapter
        }
        getListToko()
    }

    fun getListToko(){
        val params = HashMap<String,String>()
        params.put("nik", Session.getnik(this))
        HttpRequest.stringRequest(this,Config.GET_TOKO_URL,params, { s: String, json: JSONObject ->
            val rows:JSONArray = json.getJSONArray("rows")
            if (rows.length() > 0){
                for (i in 0 until rows.length()) run {
                    val row: JSONObject = rows.getJSONObject(i)
                    tokoList.add(Toko(row.getInt("id"),row.getString("nama_lokasi")))
                }
                tokoAdapter.notifyDataSetChanged()
            }
        }, {
            AppHelper.toast(this@ListTokoActivity,it,"error")
        })
    }

    override fun onItemClick(toko: Toko) {
        var i: Intent? = null
        val statusPenilaian = intent.getIntExtra("status_penilaian",0)
        if (statusPenilaian == 2){
            i = Intent(this,PenilaianActivity::class.java).apply {
                putExtra("id",toko.id)
            }

        } else {
            i = Intent(this,ListJabatanActivity::class.java).apply {
                putExtra("id",toko.id)
            }
        }

        i.putExtra("status_penilaian",statusPenilaian)

        startActivity(i)
    }
}