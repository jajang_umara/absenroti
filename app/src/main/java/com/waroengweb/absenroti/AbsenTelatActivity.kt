package com.waroengweb.absenroti

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.waroengweb.absenroti.adapter.AbsenTelatAdapter
import com.waroengweb.absenroti.model.AbsenTelat
import com.waroengweb.absenroti.utils.AppHelper
import com.waroengweb.absenroti.utils.Config
import com.waroengweb.absenroti.utils.HttpRequest
import com.waroengweb.absenroti.utils.Session
import kotlinx.android.synthetic.main.activity_absen_telat.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class AbsenTelatActivity : AppCompatActivity() {
    private var absenTelatAdapter: AbsenTelatAdapter ?= null
    private val absenList = ArrayList<AbsenTelat>()
    private lateinit var myCalendar: Calendar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_absen_telat)
        recyclerviewTelat.layoutManager = LinearLayoutManager(this)
        absenTelatAdapter = AbsenTelatAdapter(this, absenList)
        recyclerviewTelat.adapter = absenTelatAdapter
        getAbsenTelat("")
        myCalendar = Calendar.getInstance()
        tanggal_cari.setOnClickListener(View.OnClickListener {
            openDateDialog()
        })
    }

    fun getAbsenTelat(tanggal: String)
    {
        val params = HashMap<String, String>()
            params.put("tanggal",tanggal)
            params.put("nik",Session.getnik(this))
        HttpRequest.stringRequest(this, Config.URL + "api/absen_api/absen_telat", params, { s: String, json: JSONObject ->
            val rows: JSONArray = json.getJSONArray("rows")
            absenList.clear()
            if (rows.length() > 0) {

                for (i in 0 until rows.length()) run {
                    val row: JSONObject = rows.getJSONObject(i)
                    absenList.add(
                            AbsenTelat(
                                    row.getInt("id"),
                                    row.getString("tanggal"),
                                    row.getString("nama_lokasi"),
                                    row.getString("nama_lengkap"),
                                    row.getString("nama_jabatan"),
                                    row.getInt("terlambat"),
                                    row.getString("keterangan")
                            ))
                }

            }
            absenTelatAdapter?.notifyDataSetChanged()
        }, {
            AppHelper.toast(this@AbsenTelatActivity, it, "error")
        })
    }

    private fun openDateDialog() {
        DatePickerDialog(this@AbsenTelatActivity, { view, year, month, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, month)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val formatTanggal = "yyyy-MM-dd"
            val sdf = SimpleDateFormat(formatTanggal)
            tanggal_cari.setText(sdf.format(myCalendar.getTime()))
            getAbsenTelat(sdf.format(myCalendar.getTime()))

        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show()
    }
}