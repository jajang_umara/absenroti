package com.waroengweb.absenroti

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.button.MaterialButton
import com.google.android.material.button.MaterialButtonToggleGroup
import com.jaredrummler.materialspinner.MaterialSpinner
import com.waroengweb.absenroti.model.MyObject
import com.waroengweb.absenroti.utils.AppHelper
import com.waroengweb.absenroti.utils.Config
import com.waroengweb.absenroti.utils.HttpRequest
import com.waroengweb.absenroti.utils.Session
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_penilaian2.*
import org.json.JSONArray
import org.json.JSONObject

class PenilaianActivity : AppCompatActivity() {

    var listUser = ArrayList<MyObject>()
    lateinit var adapter: ArrayAdapter<MyObject>
    private var nik: Int? = null
    private var idLokasi: Int? = null
    private var idJabatan: Int? = null
    private var answer = HashMap<String, String>()
    private var keterangan: EditText? = null
    var statusPenilaian: Int? = 0;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_penilaian2)
        keterangan = EditText(this@PenilaianActivity)
        idLokasi = intent.getIntExtra("id_lokasi", 0)
        idJabatan = intent.getIntExtra("id_jabatan", 0)
        getUser()
        getListPenilaian()
        spinner.setOnItemSelectedListener(MaterialSpinner.OnItemSelectedListener<MyObject> { v: MaterialSpinner, position: Int, id: Long, item: MyObject ->
            //AppHelper.toast(this@PenilaianActivity, item.nama, "success")
            nik = item.id
        })

        statusPenilaian = intent.getIntExtra("status_penilaian",0)
        if (statusPenilaian == 2){
            spinner.visibility = View.GONE
        }

    }


    @SuppressLint("ResourceType")
    fun getListPenilaian() {
        val params = HashMap<String, String>()
            params.put("id_jabatan",idJabatan.toString())
            params.put("status_penilaian",intent.getIntExtra("status_penilaian",0).toString())
        HttpRequest.stringRequest(this, Config.GET_POINT_URL,params , { s: String, json: JSONObject ->
            val rows: JSONArray = json.getJSONArray("rows")
            if (rows.length() > 0) {

                for (i in 0 until rows.length()) run {
                    val row: JSONObject = rows.getJSONObject(i)

                    val textView = TextView(this@PenilaianActivity).apply {
                        layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)

                    }
                    val param = textView.layoutParams as ViewGroup.MarginLayoutParams
                    param.setMargins(0, 20, 0, 0)
                    textView.text = row.getString("nama")
                    textView.layoutParams = param
                    textView.textSize = 18f
                    textView.setTypeface(null, Typeface.BOLD)


                    val btnGroup = MaterialButtonToggleGroup(this@PenilaianActivity)
                    btnGroup.apply {
                        layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                    }
                    btnGroup.setSingleSelection(true)


                    val btn = MaterialButton(this, null, R.attr.materialButtonOutlinedStyle)
                    btn.apply {
                        layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                    }
                    btn.text = "Baik"
                    btn.textSize = 11f
                    btn.tag = "baik"
                    btn.icon = getDrawable(R.drawable.ic_baseline_sentiment_satisfied_alt_16)


                    val btn2 = MaterialButton(this, null, R.attr.materialButtonOutlinedStyle)
                    btn2.apply {
                        layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                    }
                    btn2.text = "Sedang"
                    btn2.tag = "sedang"
                    btn2.textSize = 11f
                    btn2.icon = getDrawable(R.drawable.ic_baseline_sentiment_satisfied_16)

                    val btn3 = MaterialButton(this, null, R.attr.materialButtonOutlinedStyle)
                    btn3.apply {
                        layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                    }
                    btn3.text = "Kurang"
                    btn3.tag = "kurang"
                    btn3.textSize = 11f
                    btn3.icon = getDrawable(R.drawable.ic_baseline_sentiment_very_dissatisfied_16)

                    btnGroup.addView(btn)
                    btnGroup.addView(btn2)
                    btnGroup.addView(btn3)

                    btnGroup.addOnButtonCheckedListener(MaterialButtonToggleGroup.OnButtonCheckedListener() { v: MaterialButtonToggleGroup, id: Int, checked: Boolean ->

                        val btn: MaterialButton? = findViewById<MaterialButton>(id)
                        answer.put(row.getInt("id").toString(), btn?.tag.toString())



                    })

                    linearLayout.addView(textView)
                    linearLayout.addView(btnGroup)
                }


                keterangan?.apply {
                    layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                }

                val param2 = keterangan?.layoutParams as ViewGroup.MarginLayoutParams
                param2.setMargins(0, 20, 0, 0)
                keterangan?.layoutParams = param2
                keterangan?.inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
                keterangan?.minLines = 5
                keterangan?.setBackgroundResource(R.drawable.shapeemail)
                keterangan?.hint = "Keterangan"
                linearLayout.addView(keterangan)


                val btnSimpan = MaterialButton(this@PenilaianActivity)
                btnSimpan.apply {
                    layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                }
                btnSimpan.text = "SIMPAN"
                val param = btnSimpan.layoutParams as ViewGroup.MarginLayoutParams
                param.setMargins(0, 20, 0, 0)
                btnSimpan.layoutParams = param
                btnSimpan.setOnClickListener(View.OnClickListener {
                    if (statusPenilaian != 2){
                        if (nik == null){
                            AppHelper.toast(this@PenilaianActivity,"Karyawan Belum Dipilih!","error")
                            return@OnClickListener
                        }
                    }


                    if (answer.size == 0){
                        AppHelper.toast(this@PenilaianActivity,"Penilaian Belum Diinput!","error")
                        return@OnClickListener
                    }

                    sendToServer()




                })
                linearLayout.addView(btnSimpan)


            }
        }, {

        })
    }

    fun getUser() {
        val params = HashMap<String, String>()
        params.put("id_lokasi", idLokasi.toString())
        params.put("id_jabatan", idJabatan.toString())

        HttpRequest.stringRequest(this, Config.GET_KARYAWAN_URL, params, { s: String, json: JSONObject ->
            listUser.clear()
            val rows: JSONArray = json.getJSONArray("rows")
            if (rows.length() > 0) {
                listUser.add(MyObject(0, "-Pilih User-"))
                for (i in 0 until rows.length()) run {
                    val row: JSONObject = rows.getJSONObject(i)
                    listUser.add(MyObject(row.getInt("id"), row.getString("nama_lengkap")))
                }
            }
            adapter = ArrayAdapter<MyObject>(this@PenilaianActivity, android.R.layout.simple_spinner_item, listUser)
            spinner.setAdapter(adapter)
        }, {
            AppHelper.toast(this@PenilaianActivity, it, "error")
        })
    }

    fun sendToServer() {
        val params = HashMap<String,String>()
            params.put("id_karyawan",nik.toString())
            params.put("nik_penilai", Session.getnik(this))
            params.put("hasil",JSONObject(answer as Map<*,*>).toString())
            params.put("keterangan",keterangan?.text.toString())
            params.put("id_lokasi",intent.getIntExtra("id",0).toString())
        HttpRequest.stringRequest(this,Config.SEND_PENILAIAN_URL,params,{m: String,json: JSONObject ->
            AppHelper.toast(this@PenilaianActivity,m,"success")
            resetForm()
        },{
            AppHelper.toast(this@PenilaianActivity,it,"error")
        })
    }

    fun resetForm(){
        for (i in 0 until linearLayout.childCount){
            var view = linearLayout.getChildAt(i)
            if (view.javaClass.name.equals("com.google.android.material.button.MaterialButtonToggleGroup")){
                var btnGroups = view as MaterialButtonToggleGroup
                btnGroups.clearChecked()
                //AppHelper.toast(this@PenilaianActivity,"Button Group","success")
            }
        }
        if (statusPenilaian != 2){
            spinner.selectedIndex = 0
        }

        nik = null
        answer.clear()
        keterangan?.text = null
    }
}