package com.waroengweb.absenroti;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.JsonReader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.gridlayout.widget.GridLayout;
import androidx.room.Room;

import com.andremion.counterfab.CounterFab;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.waroengweb.absenroti.db.AppDatabase;
import com.waroengweb.absenroti.model.AbsenTelat;
import com.waroengweb.absenroti.model.Users;
import com.waroengweb.absenroti.utils.AppHelper;
import com.waroengweb.absenroti.utils.Config;
import com.waroengweb.absenroti.utils.HttpRequest;
import com.waroengweb.absenroti.utils.Session;
import com.waroengweb.absenroti.utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.profile_image)
    ImageView profileImage;
    @BindView(R.id.mainGrid)
    GridLayout mainGrid;
    @BindView(R.id.txt_tanggal)
    TextView txtTanggal;
    @BindView(R.id.username)
    TextView username;
    @BindView(R.id.role)
    TextView role;
    @BindView(R.id.jam_masuk)
    TextView txtMasuk;
    @BindView(R.id.jam_keluar)
    TextView txtKeluar;
    @BindView(R.id.status_masuk)
    TextView stsMasuk;
    @BindView(R.id.status_keluar)
    TextView stsKeluar;
    @BindView(R.id.txt_lokasi)
    TextView txtLokasi;

    @BindView(R.id.fab_setting)
    FloatingActionButton fabSetting;

    @BindView(R.id.fab_notif)
    CounterFab fabNotif;

    private String[] permissionRequest;
    int PERMISSION_ID = 444;
    private AppDatabase db;
    private Uri filePhoto;
    private String fileString, jamMasuk, jamKeluar;
    private ProgressDialog pd;
    private JSONArray laporan;
    private int statusPenilaian = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        statusPenilaian = Session.getStatusPenilaian(this);
        setSingleGrid();
        new AppHelper(this).setDate(txtTanggal);

        permissionRequest = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_NETWORK_STATE
        };

        requestPermissions(permissionRequest);

        Glide.with(getApplicationContext())
                .load(getResources().getDrawable(R.drawable.boy))
                .apply(RequestOptions.circleCropTransform())
                .into(profileImage);

        db = Room.databaseBuilder(this,
                AppDatabase.class, "MyDB").allowMainThreadQueries().fallbackToDestructiveMigration().build();

        setLabelDashboard();
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        getProfile();
        setNotif();

    }

    void setNotif(){
        if (Session.getStatusPenilaian(this) == 0){
            fabNotif.setVisibility(View.GONE);
        }

    }

    void setLabelDashboard() {
        username.setText(Session.getFullname(this));
        role.setText(Session.getJabatan(this));
        txtLokasi.setText(Session.getLokasi(this));

    }


    public void setSingleGrid() {
        for (int i = 0; i < mainGrid.getChildCount(); i++) {
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            if (i == 2) {
                if (statusPenilaian == 0) {
                    cardView.setEnabled(false);
                }
            }
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent;
                    if (finalI == 0) {
                        //intent = new Intent(MainActivity.this,Absensi.class);
                        //startActivity(intent);
                        showPopupMenu(view, true, R.style.MyPopupStyle);

                    } else if (finalI == 1) {
                        //intent = new Intent(MainActivity.this, Dokumentasi.class);
                        //startActivity(intent);
                        showPopupMenu2(view, true, R.style.MyPopupStyle);
                    } else if (finalI == 2) {
                        intent = new Intent(MainActivity.this, ListTokoActivity.class);
                        intent.putExtra("status_penilaian",statusPenilaian);
                        startActivity(intent);
                    } else if (finalI == 3) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                MainActivity.this);

                        // set title
                        alertDialogBuilder.setTitle("Pertanyaan");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Klik Ya Untuk keluar")
                                .setCancelable(false)
                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().commit();
                                        android.os.Process.killProcess(android.os.Process.myPid());
                                        System.exit(0);
                                    }
                                })
                                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // if this button is clicked, just close
                                        // the dialog box and do nothing
                                        dialog.cancel();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }
                }
            });
        }

    }


    private void requestPermissions(String[] permissionRequests) {
        ActivityCompat.requestPermissions(
                this,
                permissionRequests,
                PERMISSION_ID
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getProfile();
    }

    private void showPopupMenu(View anchor, boolean isWithIcons, int style) {
        //init the wrapper with style
        Context wrapper = new ContextThemeWrapper(this, style);

        //init the popup
        PopupMenu popup = new PopupMenu(wrapper, anchor);

        /*  The below code in try catch is responsible to display icons*/
        if (isWithIcons) {
            try {
                Field[] fields = popup.getClass().getDeclaredFields();
                for (Field field : fields) {
                    if ("mPopup".equals(field.getName())) {
                        field.setAccessible(true);
                        Object menuPopupHelper = field.get(popup);
                        Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
                        Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
                        setForceIcons.invoke(menuPopupHelper, true);
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //inflate menu
        popup.getMenuInflater().inflate(R.menu.absen_sub_menu, popup.getMenu());

        //implement click events
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intent;
                switch (menuItem.getItemId()) {
                    case R.id.absen:
                        intent = new Intent(MainActivity.this, Absensi.class);
                        intent.putExtra("jam_masuk", jamMasuk);
                        intent.putExtra("jam_keluar", jamKeluar);
                        startActivity(intent);
                        break;
                    case R.id.list_absen:
                        intent = new Intent(MainActivity.this, AbsenListActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.absen_lembur:
                        intent = new Intent(MainActivity.this, LemburActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.absen_telat:
                        if (statusPenilaian != 0){
                            intent = new Intent(MainActivity.this, AbsenTelatActivity.class);
                            intent.putExtra("absensi",1);
                            startActivity(intent);
                            break;
                        }

                }
                return true;
            }
        });
        popup.show();

    }

    private void showPopupMenu2(View anchor, boolean isWithIcons, int style) {
        //init the wrapper with style
        Context wrapper = new ContextThemeWrapper(this, style);

        //init the popup
        PopupMenu popup = new PopupMenu(wrapper, anchor);

        /*  The below code in try catch is responsible to display icons*/
        if (isWithIcons) {
            try {
                Field[] fields = popup.getClass().getDeclaredFields();
                for (Field field : fields) {
                    if ("mPopup".equals(field.getName())) {
                        field.setAccessible(true);
                        Object menuPopupHelper = field.get(popup);
                        Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
                        Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
                        setForceIcons.invoke(menuPopupHelper, true);
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //inflate menu
        //popup.getMenuInflater().inflate(R.menu.laporan_sub_menu, popup.getMenu());
        if (laporan != null) {
            if (laporan.length() > 0) {
                for (int i = 0; i < laporan.length(); i++) {
                    try {
                        JSONObject row = laporan.getJSONObject(i);
                        popup.getMenu().add(Menu.NONE, row.getInt("id"), row.getInt("id"), row.getString("nama"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
        if (statusPenilaian != 0) {
            popup.getMenu().add(Menu.NONE, 0, 0, "Laporan Telat");
        }

        //implement click events
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intent;
                if (menuItem.getItemId() == 0){
                    intent = new Intent(MainActivity.this, LaporanTelatActivity.class);
                } else {
                    intent = new Intent(MainActivity.this, LaporanActivity.class);
                    intent.putExtra("id", menuItem.getItemId());
                }

                startActivity(intent);
                return true;
            }
        });
        popup.show();

    }

    @OnClick(R.id.fab_profile)
    public void profileClick() {
        Intent i = new Intent(MainActivity.this, ProfileActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.fab_slip)
    public void slipClick() {
        Intent i = new Intent(MainActivity.this, SlipActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.fab_notif)
    public void notifClick() {
        Intent intent = new Intent(MainActivity.this, AbsenTelatActivity.class);
        intent.putExtra("absensi",1);
        startActivity(intent);
    }

    @OnClick(R.id.fab_setting)
    void changePassword() {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(this);
        View mView = layoutInflaterAndroid.inflate(R.layout.user_input_dialog_box, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(this);
        alertDialogBuilderUserInput.setView(mView);

        final EditText oldPassword = (EditText) mView.findViewById(R.id.old_password);
        final EditText newPassword = (EditText) mView.findViewById(R.id.new_password);
        final EditText confirmPassword = (EditText) mView.findViewById(R.id.confirm_password);

        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        if (oldPassword.getText().toString().equals("")) {
                            AppHelper.toast(MainActivity.this, "Password Lama Masih Kosong", "error");
                            return;
                        }

                        if (newPassword.getText().toString().equals("")) {
                            AppHelper.toast(MainActivity.this, "Password Baru Masih Kosong", "error");
                            return;
                        }

                        if (!newPassword.getText().toString().equals(confirmPassword.getText().toString())) {
                            AppHelper.toast(MainActivity.this, "Password Tidak Sama", "error");
                            return;
                        }

                        Map<String, String> params = new HashMap<>();
                        params.put("nik", Session.getnik(MainActivity.this));
                        params.put("old_password", oldPassword.getText().toString());
                        params.put("new_password", newPassword.getText().toString());

                        HttpRequest.stringRequest(MainActivity.this, Config.CHANGE_PASSWORD_URL, params,
                                new HttpRequest.ResponseSuccess() {
                                    @Override
                                    public void onSuccess(String message, JSONObject json) {
                                        AppHelper.toast(MainActivity.this, message, "success");
                                        dialogBox.dismiss();
                                    }
                                }, new HttpRequest.ResponseError() {
                                    @Override
                                    public void onError(String message) {
                                        AppHelper.toast(MainActivity.this, message, "error");
                                    }
                                });
                    }
                })

                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();
    }

    @OnClick(R.id.profile_image)
    public void imageClick() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent i;
            i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File pictureFile = null;
            try {
                pictureFile = new AppHelper(this).getOutputMediaFile();
            } catch (IOException ex) {
                Toast.makeText(this,
                        "Photo file can't be created, please try again",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            filePhoto = Uri.fromFile(pictureFile);
            Uri photoUri = FileProvider.getUriForFile(this,
                    "com.waroengweb.absenroti.fileprovider", pictureFile
            );
            i.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

            startActivityForResult(i, 300);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            File compressPhoto = compressImage(filePhoto);
            fileString = (compressImage(filePhoto)).toString();
            Glide.with(this).load(fileString).circleCrop().into(profileImage);
            updateprofile();
        }

    }

    public File compressImage(Uri fileData) {

        File compressFile;
        try {
            compressFile = new Compressor(this).compressToFile(new File(fileData.getPath()));
            return compressFile;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void updateprofile() {
        db.UserDao().updatePhoto(fileString, Session.getnik(this));
        Glide.with(MainActivity.this).load(fileString).circleCrop().into(profileImage);
    }

    void getProfile() {
        //pd.show();
        Map<String, String> params = new HashMap<>();
        params.put("nik", Session.getnik(this));
        HttpRequest.stringRequest(this, Config.PROFILE_URL, params, new HttpRequest.ResponseSuccess() {
            @Override
            public void onSuccess(String message, JSONObject json) {
                //pd.hide();
                try {
                    Users user = db.UserDao().getUser(Session.getnik(MainActivity.this));
                    if (user == null) {
                        Glide.with(MainActivity.this).load(R.drawable.avatar).circleCrop().into(profileImage);
                        user = new Users();
                        user.setNik(Session.getnik(MainActivity.this));
                        db.UserDao().insertUser(user);
                    } else {
                        Glide.with(MainActivity.this).load(user.getPhoto()).circleCrop().into(profileImage);
                    }

                    laporan = json.getJSONArray("laporan");

                    txtMasuk.setText(json.getString("jam_masuk"));
                    txtKeluar.setText(json.getString("jam_keluar"));
                    int terlambat = json.getInt("terlambat");

                    jamMasuk = json.getString("jam_masuk");
                    jamKeluar = json.getString("jam_keluar");

                    fabNotif.setCount(json.getInt("jumlah_telat"));

                    if (json.getString("jam_masuk").equals("0:00")) {
                        stsMasuk.setText("Belum Absen");
                        txtMasuk.setText("-");
                    } else {
                        if (terlambat > 10) {
                            stsMasuk.setText("Telat");
                            stsMasuk.setTextColor(Color.RED);
                        } else {
                            stsMasuk.setText("Berhasil");
                            stsMasuk.setTextColor(Color.GREEN);
                        }
                    }

                    if (json.getString("jam_keluar").equals("0:00")) {
                        stsKeluar.setText("Belum Absen");
                        txtKeluar.setText("-");
                    } else {
                        stsKeluar.setText("Berhasil");
                        stsKeluar.setTextColor(Color.GREEN);
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new HttpRequest.ResponseError() {
            @Override
            public void onError(String message) {
                AppHelper.toast(MainActivity.this,message,"error");
                Handler handler = new Handler();

                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                      finish();
                    }

                }, 2000);
            }
        });
    }

    @Override
    public void onBackPressed() {
        AppHelper.toast(this, "Silakan Pilih Logout untuk keluar dari aplikasi", "error");
    }

}