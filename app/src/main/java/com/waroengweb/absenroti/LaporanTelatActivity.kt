package com.waroengweb.absenroti

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.waroengweb.absenroti.adapter.AbsenTelatAdapter
import com.waroengweb.absenroti.adapter.LaporanTelatAdapter
import com.waroengweb.absenroti.model.AbsenTelat
import com.waroengweb.absenroti.model.LaporanTelat
import com.waroengweb.absenroti.utils.AppHelper
import com.waroengweb.absenroti.utils.Config
import com.waroengweb.absenroti.utils.HttpRequest
import com.waroengweb.absenroti.utils.Session
import kotlinx.android.synthetic.main.activity_absen_telat.*
import kotlinx.android.synthetic.main.activity_laporan_telat.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class LaporanTelatActivity : AppCompatActivity() {
    private var absenTelatAdapter: LaporanTelatAdapter?= null
    private val absenList = ArrayList<LaporanTelat>()
    private lateinit var myCalendar: Calendar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_laporan_telat)
        recyclerviewLaporan.layoutManager = LinearLayoutManager(this)
        absenTelatAdapter = LaporanTelatAdapter(this, absenList)
        recyclerviewLaporan.adapter = absenTelatAdapter
        getLaporanTelat("")
        myCalendar = Calendar.getInstance()
        tanggal_cari_2.setOnClickListener(View.OnClickListener {
            openDateDialog()
        })
    }

    fun getLaporanTelat(tanggal: String)
    {
        val params = HashMap<String, String>()
        params.put("tanggal",tanggal)
        params.put("nik", Session.getnik(this))
        HttpRequest.stringRequest(this, Config.URL + "api/laporan_api/laporan_telat", params, { s: String, json: JSONObject ->
            val rows: JSONArray = json.getJSONArray("rows")
            absenList.clear()
            if (rows.length() > 0) {

                for (i in 0 until rows.length()) run {
                    val row: JSONObject = rows.getJSONObject(i)
                    absenList.add(
                            LaporanTelat(
                                    row.getInt("id"),
                                    row.getString("tanggal"),
                                    row.getString("nama_lokasi"),
                                    row.getString("nama_lengkap"),
                                    row.getString("nama_jabatan"),
                                    row.getInt("terlambat"),
                                    row.getString("nama_penilaian")
                            ))
                }

            }
            absenTelatAdapter?.notifyDataSetChanged()
        }, {
            AppHelper.toast(this@LaporanTelatActivity, it, "error")
        })
    }

    private fun openDateDialog() {
        DatePickerDialog(this@LaporanTelatActivity, { view, year, month, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, month)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val formatTanggal = "yyyy-MM-dd"
            val sdf = SimpleDateFormat(formatTanggal)
            tanggal_cari_2.setText(sdf.format(myCalendar.getTime()))
            getLaporanTelat(sdf.format(myCalendar.getTime()))

        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show()
    }
}