package com.waroengweb.absenroti

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.waroengweb.absenroti.adapter.JabatanAdapter
import com.waroengweb.absenroti.model.MyObject
import com.waroengweb.absenroti.utils.AppHelper
import com.waroengweb.absenroti.utils.Config
import com.waroengweb.absenroti.utils.HttpRequest
import kotlinx.android.synthetic.main.activity_list_jabatan.*
import org.json.JSONArray
import org.json.JSONObject

class ListJabatanActivity : AppCompatActivity(),JabatanAdapter.OnClickCard {
    private var id:Int? = null
    private lateinit var jabatanAdapter: JabatanAdapter
    private var list = ArrayList<MyObject>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_jabatan)
        id = intent.getIntExtra("id",0)
        jabatanAdapter = JabatanAdapter(list)
        jabatanAdapter.setOnClickListener(this)
        recyclerviewJabatan.apply {
            layoutManager = GridLayoutManager(this@ListJabatanActivity,2)
            adapter = jabatanAdapter
        }
        getListJabatan()
    }

    fun getListJabatan(){
        val params = HashMap<String,String>()
            params.put("id",id.toString())
        HttpRequest.stringRequest(this, Config.GET_JABATAN_URL,params, { s: String, json: JSONObject ->
            val rows: JSONArray = json.getJSONArray("rows")
            if (rows.length() > 0){
                for (i in 0 until rows.length()) run {
                    val row: JSONObject = rows.getJSONObject(i)
                    list.add(MyObject(row.getInt("id"),row.getString("nama_jabatan")))
                }
                jabatanAdapter.notifyDataSetChanged()
            }
        }, {
            AppHelper.toast(this@ListJabatanActivity,it,"error")
        })
    }

    override fun onItemClick(myObject: MyObject) {
        val intent = Intent(this@ListJabatanActivity,PenilaianActivity::class.java)
        intent.putExtra("id_lokasi",id)
        intent.putExtra("id_jabatan",myObject.id)
        startActivity(intent)
    }
}