package com.waroengweb.absenroti;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.amulyakhare.textdrawable.TextDrawable;
import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.waroengweb.absenroti.utils.AppController;
import com.waroengweb.absenroti.utils.AppHelper;
import com.waroengweb.absenroti.utils.Config;
import com.waroengweb.absenroti.utils.HttpRequest;
import com.waroengweb.absenroti.utils.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AbsenListActivity extends AppCompatActivity {

    @BindView(R.id.calendarView)
    CalendarView calendarView;
    List<EventDay> events = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absen_list);
        getSupportActionBar().setTitle("History Absensi");
        ButterKnife.bind(this);
        Calendar c = Calendar.getInstance();

        int bulan = c.get(Calendar.MONTH);
        getAbsen(bulan);

		calendarView.setOnPreviousPageChangeListener(new OnCalendarPageChangeListener() {
			@Override
			public void onChange() {
        		Calendar c = calendarView.getCurrentPageDate();
        		int bulan = c.get(Calendar.MONTH);
        		getAbsen(bulan);
			}
		});

		calendarView.setOnForwardPageChangeListener(new OnCalendarPageChangeListener() {
			@Override
			public void onChange() {
				Calendar c = calendarView.getCurrentPageDate();
				int bulan = c.get(Calendar.MONTH);
				getAbsen(bulan);
			}
		});

		calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                Calendar day = eventDay.getCalendar();
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                Map<String, String> params = new HashMap<>();
                params.put("nik", Session.getnik(AbsenListActivity.this));
                params.put("tanggal",format1.format(day.getTime()));
                HttpRequest.stringRequest(AbsenListActivity.this, Config.GET_ABSEN_URL, params, new HttpRequest.ResponseSuccess() {
                    @Override
                    public void onSuccess(String message, JSONObject json) throws JSONException {
                        AppHelper.toast(AbsenListActivity.this,"Masuk : "+json.getString("jam_masuk")+" Keluar : "+json.getString("jam_keluar"),"info");
                    }
                }, new HttpRequest.ResponseError() {
                    @Override
                    public void onError(String message) {
                        AppHelper.toast(AbsenListActivity.this,message,"error");
                    }
                });
            }
        });

    }

    void getAbsen(int month) {
        Map<String, String> params = new HashMap<>();
        params.put("nik", Session.getnik(this));
        params.put("bulan", String.valueOf(month + 1));
        HttpRequest.stringRequest(this, Config.ABSEN_LIST_URL, params, new HttpRequest.ResponseSuccess() {
            @Override
            public void onSuccess(String message, JSONObject json) {
                try {
                    JSONArray absen = json.getJSONArray("absen");
                    if (absen.length() > 0) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        String jamMasuk;
                        String jamKeluar;
                        for (int i = 0; i < absen.length(); i++) {
                            JSONObject row = absen.getJSONObject(i);
                            jamMasuk = row.getString("jam_masuk");
                            jamKeluar = row.getString("jam_keluar");
                            if (jamMasuk.equals("0:00")) {
                                jamMasuk = "";
                            }

                            if (jamKeluar.equals("0:00")) {
                                jamKeluar = "";
                            }

                            TextDrawable drawable = TextDrawable.builder()
                                    .beginConfig()
                                    .fontSize(10)
                                    .textColor(Color.BLACK)
                                    .bold()
                                    .endConfig()
                                    .buildRect(jamMasuk + "-" + jamKeluar, Color.WHITE);

                            Calendar c = Calendar.getInstance();
                            try {
                                c.setTime(sdf.parse(row.getString("tanggal")));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            events.add(new EventDay(c, drawable));
                        }
                        calendarView.setEvents(events);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new HttpRequest.ResponseError() {
            @Override
            public void onError(String message) {
                AppHelper.toast(AbsenListActivity.this,message,"error");
            }
        });
    }
}