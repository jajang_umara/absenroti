package com.waroengweb.absenroti;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.waroengweb.absenroti.adapter.ImageAdapter;
import com.waroengweb.absenroti.model.Image;
import com.waroengweb.absenroti.model.SubPoint;
import com.waroengweb.absenroti.utils.AppHelper;
import com.waroengweb.absenroti.utils.Config;
import com.waroengweb.absenroti.utils.HttpRequest;
import com.waroengweb.absenroti.utils.Session;
import com.waroengweb.absenroti.utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;

public class Dokumentasi extends AppCompatActivity implements ImageAdapter.OnDeleteImage {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;

    private List<Image> imageList = new ArrayList<>();
    private ImageAdapter imageAdapter;
    private Uri filePhoto;
    private String fileString;
    private ProgressDialog pd;
    private String type;
    private List<SubPoint> pointList = new ArrayList<>();
    private ArrayAdapter<SubPoint> adapter;
    private int idSubpoint = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dokumentasi);
        ButterKnife.bind(this);
        imageAdapter = new ImageAdapter(this, imageList);
        imageAdapter.setOnDeleteImage(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(imageAdapter);
        type = getIntent().getStringExtra("jenis");
        getSupportActionBar().setTitle("Laporan " + type);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        idSubpoint = getIntent().getIntExtra("id_penilaian",1);


    }



    @OnClick(R.id.fab_add)
    public void addImage() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent i;
            i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File pictureFile = null;
            try {
                pictureFile = new AppHelper(this).getOutputMediaFile();
            } catch (IOException ex) {
                Toast.makeText(this,
                        "Photo file can't be created, please try again",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            filePhoto = Uri.fromFile(pictureFile);
            Uri photoUri = FileProvider.getUriForFile(this,
                    "com.waroengweb.absenroti.fileprovider", pictureFile
            );
            i.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

            startActivityForResult(i, 300);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            File compressPhoto = compressImage(filePhoto);
            fileString = (compressImage(filePhoto)).toString();
            Image image = new Image();
            image.setImage(fileString);
            imageList.add(image);
            imageAdapter.notifyDataSetChanged();
        }

    }

    public File compressImage(Uri fileData) {

        File compressFile;
        try {
            compressFile = new Compressor(this).compressToFile(new File(fileData.getPath()));
            return compressFile;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void onClickButton(Image image, int position) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Apakah Yakin menghapus Photo ini?.");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        File imageFile = new File(image.getImage());
                        if (imageFile.exists()) {
                            imageFile.delete();
                        }
                        imageList.remove(position);
                        imageAdapter.notifyItemRemoved(position);
                        imageAdapter.notifyItemRangeChanged(position, imageList.size());
                        dialog.cancel();
                    }

                    //
                });
        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @OnClick(R.id.fab_upload)
    void uploadPhoto() {
        if (imageList.size() == 0){
            AppHelper.toast(this,"Photo Masih Kosong","error");
            return;
        }

        if (idSubpoint == 0){
            AppHelper.toast(this,"Sub point Penilaian Belum dipilih","error");
            return;
        }

        pd.show();

        Map<String,String> params = new HashMap<>();

        params.put("nik", Session.getnik(this));
        params.put("sub",String.valueOf(idSubpoint));
        Map<String, VolleyMultipartRequest.DataPart> files = new HashMap<>();
        for (int i=0;i<imageList.size();i++){
            long imagename = System.currentTimeMillis();
            Image image = imageList.get(i);
            files.put("photo["+i+"]",new VolleyMultipartRequest.DataPart("PIC_1_" + imagename + ".jpg", new AppHelper(this).convertImageToBytes(image.getImage())));
        }

        HttpRequest.multiPartRequest(this, Config.UPLOAD_LAPORAN_URL, params, files, new HttpRequest.ResponseSuccess() {
            @Override
            public void onSuccess(String message, JSONObject json) {
                pd.hide();
                //AppHelper.toast(Dokumentasi.this,message,"success");
                //imageList.clear();
                //imageAdapter.notifyDataSetChanged();
                onBackPressed();
            }
        }, new HttpRequest.ResponseError() {
            @Override
            public void onError(String message) {
                pd.hide();
                AppHelper.toast(Dokumentasi.this,message,"error");
            }
        });
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if ( pd!=null && pd.isShowing() ){
            pd.cancel();
        }
    }
}