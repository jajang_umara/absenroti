package com.waroengweb.absenroti.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
public class HttpRequest {

    public static void stringRequest(Context context,String url, Map<String,String> params, ResponseSuccess responseSuccess, ResponseError responseError){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject json;
                Log.d("response :",response);
                try {
                    json = new JSONObject(response);
                    if (json.getBoolean("error") == false){
                        JSONObject data = json.getJSONObject("data");
                        responseSuccess.onSuccess(json.getString("message"),data);
                    } else {
                        responseError.onError(json.getString("message"));
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NoConnectionError){
                    responseError.onError("Tidak Ada Jaringan Internet");
                } else if (error instanceof TimeoutError) {
                    responseError.onError("Jaringan Internet Bermasalah Silakan coba beberapa saat lagi");
                } else {
                    responseError.onError("Error Response Server");
                    String body;
                    try {
                        body = new String(error.networkResponse.data,"UTF-8");
                        Log.d("Error :",body);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        }){
            @Override
            protected Map<String, String> getParams(){
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization", Session.getToken(context));

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public static void multiPartRequest(Context context, String url, Map<String,String> params, Map<String, VolleyMultipartRequest.DataPart> files, ResponseSuccess responseSuccess, ResponseError responseError){
        VolleyMultipartRequest multiReq = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse result) {
                Log.d("Response :",new String(result.data));
                if (result.statusCode == 200){
                    String response = new String(result.data);
                    JSONObject json;
                    try {
                        json = new JSONObject(response);
                        if (json.getBoolean("error") == false){
                            JSONObject data = json.getJSONObject("data");
                            responseSuccess.onSuccess(json.getString("message"),data);
                        } else {
                            responseError.onError(json.getString("message"));
                        }
                    } catch (JSONException e){
                        e.printStackTrace();

                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d("Response :",error.toString());

                if (error instanceof NoConnectionError){
                    responseError.onError("Tidak Ada Jaringan Internet");
                } else if (error instanceof TimeoutError) {
                    responseError.onError("Jaringan Internet Bermasalah Silakan coba beberapa saat lagi");
                } else {
                    responseError.onError("Error Response Server");
                    String body;
                    try {
                        body = new String(error.networkResponse.data,"UTF-8");
                        Log.d("Error :",body);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }

            }
        }){
            @Override
            protected Map<String, String> getParams(){
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError {
                return files;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization", Session.getToken(context));

                return params;
            }
        };

        multiReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(multiReq);
    }

    public interface ResponseSuccess {
        public void onSuccess(String message,JSONObject json) throws JSONException;
    }
		
    public interface ResponseError {
        public void onError(String message);
    }
}
