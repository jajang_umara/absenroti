package com.waroengweb.absenroti.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by OVAY on 1/4/2019.
 */

public class Session {


    static final String LOGIN_STATUS = "login_status";
    static final String USER_ID = "user_id";
    static final String ROLE = "role";
    static final String FULLNAME = "fullname";
    static final String TOKEN = "token";
    static final String NIK = "nik";
    static final String LOKASI = "lokasi";
    static final String LOKASI_ID = "lokasi_id";
    static final String STATUS_PENILAIAN = "status_penilaian";
    static final String LATITUDE = "latitude";
    static final String LONGITUDE = "longitude";
    static final String RADIUS = "radius";
    static final String JABATAN = "jabatan";

    public static SharedPreferences getSharedPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }


    public static boolean getLoginStatus(Context context) {
        return getSharedPreference(context).getBoolean(LOGIN_STATUS, false);
    }

    public static void setLoginStatus(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putBoolean(LOGIN_STATUS, status);
        editor.apply();
    }

    public static int getUserId(Context context) {
        return getSharedPreference(context).getInt(USER_ID, 0);
    }

    public static void setUserId(Context context, int id) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putInt(USER_ID, id);
        editor.apply();
    }

    public static int getRadius(Context context) {
        return getSharedPreference(context).getInt(RADIUS, 0);
    }

    public static void setRadius(Context context, int radius) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putInt(RADIUS, radius);
        editor.apply();
    }

    public static int getStatusPenilaian(Context context) {
        return getSharedPreference(context).getInt(STATUS_PENILAIAN, 0);
    }

    public static void setStatusPenilaian(Context context, int id) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putInt(STATUS_PENILAIAN, id);
        editor.apply();
    }

    public static String getRole(Context context) {
        return getSharedPreference(context).getString(ROLE, "");
    }

    public static void setRole(Context context, String role) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(ROLE, role);
        editor.apply();
    }

    public static String getJabatan(Context context) {
        return getSharedPreference(context).getString(JABATAN, "");
    }

    public static void setJabatan(Context context, String jabatan) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(JABATAN, jabatan);
        editor.apply();
    }

    public static String getFullname(Context context) {
        return getSharedPreference(context).getString(FULLNAME, "");
    }

    public static void setFullname(Context context, String fullname) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(FULLNAME, fullname);
        editor.apply();
    }

    public static String getToken(Context context) {
        return getSharedPreference(context).getString(TOKEN, "");
    }

    public static void setToken(Context context, String token) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(TOKEN, token);
        editor.apply();
    }

    public static String getnik(Context context) {
        return getSharedPreference(context).getString(NIK, "");
    }

    public static void setNik(Context context, String nik) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(NIK, nik);
        editor.apply();
    }

    public static String getLokasi(Context context) {
        return getSharedPreference(context).getString(LOKASI, "");
    }

    public static void setLokasi(Context context, String lokasi) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(LOKASI, lokasi);
        editor.apply();
    }

    public static int getLokasiId(Context context) {
        return getSharedPreference(context).getInt(LOKASI_ID, 0);
    }

    public static void setLokasiId(Context context, int idLokasi) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putInt(LOKASI_ID, idLokasi);
        editor.apply();
    }

    public static void setLatitude(Context context,Double latitude)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        putDouble(editor,LATITUDE, latitude);
        editor.apply();
    }

    public static Double getLatitude(Context context)
    {
        return getDouble(getSharedPreference(context),LATITUDE,0);
    }

    public static void setLongitude(Context context,Double longitude)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        putDouble(editor,LONGITUDE, longitude);
        editor.apply();
    }

    public static Double getLongitude(Context context)
    {
        return getDouble(getSharedPreference(context),LONGITUDE,0);
    }

    static SharedPreferences.Editor putDouble(final SharedPreferences.Editor edit, final String key, final double value) {
        return edit.putLong(key, Double.doubleToRawLongBits(value));
    }

    static double getDouble(final SharedPreferences prefs, final String key, final double defaultValue) {
        return Double.longBitsToDouble(prefs.getLong(key, Double.doubleToLongBits(defaultValue)));
    }
}
