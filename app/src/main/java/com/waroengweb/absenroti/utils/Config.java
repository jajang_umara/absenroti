package com.waroengweb.absenroti.utils;

public class Config {
    final public static String URL = "http://appskhasanahsari.com/";
    final public static String LOGIN_URL = URL+"api/login_api";
    final public static String ABSEN_URL = URL+"api/absen_api";
    final public static String PROFILE_URL = URL+"api/users_api/get_profile";
    final public static String PROFILE_UPDATE_URL = URL+"api/users_api/update_profile";
    final public static String ABSEN_LIST_URL = URL+"api/absen_api/get_absen";
    final public static String UPLOAD_LAPORAN_URL = URL+"api/laporan_api/upload";
    final public static String CHANGE_PASSWORD_URL = URL+"api/users_api/change_password";
    final public static String GET_MENU_URL = URL+"api/laporan_api/get_menu";
    final public static String GET_TOKO_URL = URL+"api/laporan_api/get_toko";
    final public static String GET_JABATAN_URL = URL+"api/laporan_api/get_jabatan";
    final public static String GET_KARYAWAN_URL = URL+"api/users_api/get_karyawan";
    final public static String GET_POINT_URL = URL+"api/laporan_api/get_penilaian";
    final public static String SEND_PENILAIAN_URL = URL+"api/penilaian_api/save";
    final public static String GET_ABSEN_URL = URL+"api/absen_api/absen";
}
