package com.waroengweb.absenroti.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

public class AppHelper {
    Context context;

    public AppHelper(Context context){
        this.context = context;
    }

    public File getOutputMediaFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String mFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File mFile = File.createTempFile(mFileName, ".jpg", storageDir);
        return mFile;
    }

    public void setDate(TextView txtTanggal) {
        String tanggal = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        String[] dayName = new String[]{"Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu"};
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int dayWeek = calendar.get(Calendar.DAY_OF_WEEK);
        txtTanggal.setText(dayName[dayWeek-1]+", "+tanggal);
    }

    public  byte[] convertImageToBytes(String path) {
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public static void toast(Context context, String message,String type) {
        if (type == "success"){
            Toasty.success(context, message, Toast.LENGTH_SHORT,true).show();
        } else if (type == "error"){
            Toasty.error(context, message, Toast.LENGTH_SHORT,true).show();
        } else {
            Toasty.normal(context, message, Toast.LENGTH_SHORT).show();
        }

    }
}
