package com.waroengweb.absenroti

import android.app.DatePickerDialog
import android.os.Bundle
import android.os.Process
import android.preference.PreferenceManager
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.waroengweb.absenroti.adapter.LemburAdapter
import com.waroengweb.absenroti.model.Lembur
import com.waroengweb.absenroti.utils.AppHelper
import com.waroengweb.absenroti.utils.Config
import com.waroengweb.absenroti.utils.HttpRequest
import com.waroengweb.absenroti.utils.Session
import kotlinx.android.synthetic.main.activity_absen_telat.*
import kotlinx.android.synthetic.main.activity_lembur.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class LemburActivity : AppCompatActivity(),LemburAdapter.OnClickButton {
    private var lemburAdapter: LemburAdapter?= null
    private val absenList = ArrayList<Lembur>()
    private lateinit var myCalendar: Calendar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lembur)
        recyclerviewLembur.layoutManager = LinearLayoutManager(this)
        lemburAdapter = LemburAdapter(this, absenList)
        lemburAdapter?.setOnClickButton(this)
        recyclerviewLembur.adapter = lemburAdapter
        getLembur("")
        myCalendar = Calendar.getInstance()
        tanggal_cari_lembur.setOnClickListener(View.OnClickListener {
            openDateDialog()
        })
    }
    fun getLembur(tanggal: String)
    {
        val params = HashMap<String, String>()
        params.put("tanggal", tanggal)
        if (Session.getStatusPenilaian(this) == 0){
            params.put("nik",Session.getnik(this))
        }
        HttpRequest.stringRequest(this, Config.URL + "api/absen_api/lembur", params, { s: String, json: JSONObject ->
            val rows: JSONArray = json.getJSONArray("rows")
            absenList.clear()
            if (rows.length() > 0) {

                for (i in 0 until rows.length()) run {
                    val row: JSONObject = rows.getJSONObject(i)
                    absenList.add(
                            Lembur(
                                    row.getInt("id"),
                                    row.getString("tanggal"),
                                    row.getString("nama_lokasi"),
                                    row.getString("nama_lengkap"),
                                    row.getString("nama_jabatan"),
                                    row.getInt("lembur"),
                                    row.getInt("acc_lembur")
                            ))
                }

            }
            lemburAdapter?.notifyDataSetChanged()
        }, {
            AppHelper.toast(this@LemburActivity, it, "error")
        })
    }

    private fun openDateDialog() {
        DatePickerDialog(this@LemburActivity, { view, year, month, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, month)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val formatTanggal = "yyyy-MM-dd"
            val sdf = SimpleDateFormat(formatTanggal)
            tanggal_cari_lembur.setText(sdf.format(myCalendar.getTime()))
            getLembur(sdf.format(myCalendar.getTime()))

        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show()
    }

    override fun onClickButton(lembur: Lembur, position: Int) {
        val alertDialogBuilder = AlertDialog.Builder(
                this@LemburActivity)

        alertDialogBuilder.setTitle("Pertanyaan")

        alertDialogBuilder
                .setMessage("Apakah Yakin Acc Lembur ini")
                .setCancelable(false)
                .setPositiveButton("Ya") { dialog, id ->
                    accLembur(lembur.id)
                }
                .setNegativeButton("Tidak") { dialog, id -> // if this button is clicked, just close
                    // the dialog box and do nothing
                    dialog.cancel()
                }

        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }

    fun accLembur(id: Int)
    {
        val params = HashMap<String, String>()
        params.put("id", id.toString())
        params.put("nik",Session.getnik(this))
        HttpRequest.stringRequest(this, Config.URL + "api/absen_api/acc_lembur", params, { s: String, json: JSONObject ->
            if(s.equals("success")){
                AppHelper.toast(this@LemburActivity, "Lembur Berhasil Diacc", "success")
                getLembur("")
            } else {
                AppHelper.toast(this@LemburActivity, "Lembur Gagal Diacc", "error")
            }
        }, {
            AppHelper.toast(this@LemburActivity, it, "error")
        })
    }
}